# Bendystraw

A web application for generating Bennedict Cumberbatch names.

## Credit

The names contained in `names.json` are sourced from [here](https://benedictcumberbatchgenerator.tumblr.com/).

## Usage

### Create a venv
### Install requirements
### Deploy like any other Flask app

## Try it

This app is hosted at https://bendystraw.katelyn.dev
