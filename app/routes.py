from flask import render_template
from app import app
import bendystraw

@app.route('/')
@app.route('/index')
def index():
    name = bendystraw.name()
    return render_template('index.html', name=name)
