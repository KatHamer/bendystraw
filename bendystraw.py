"""
Bendystraw
Generates names for Bennedict Cucumber
By Katelyn Hamer
"""

from pathlib import Path
import json
import random

NAMES_FILE = "names.json"

def name():
    """Generate a name"""
    names_path = Path(NAMES_FILE)
    with open(names_path) as fp:
        names = json.load(fp)
    first_name, last_name = random.choice(names.get("first_names")), random.choice(names.get("last_names"))
    generated_name = f"{first_name} {last_name}"
    return generated_name
